(define-key message-mode-map "\C-cs" 'message-templ-select)

(setq message-templ-config-alist '(("^To:.*notmuch@notmuchmail.org"
                                    (lambda ()
                                      (message-templ-apply "alternate"))
				    ("X-List-To" . "notmuch"))))

(setq message-templ-config-alist '(("^To:.*notmuch@notmuchmail.org" my-func)))

(setq message-templ-alist '(("alternate"
			     ("From" . "David Bremner <david@example.com>")
			     ("Bcc" . "david@example.com"))
			    ("student-inquiry"
			     ("Subject" . "Your inquiry about foo")
			     (body-file . "~/config/dat/student09.txt"))
			    ("course-marks"
			     ("Subject" . "Marks for CSXXXX")
			     ("Bcc" . "david@example.com")
			     (top-file . "~/teaching/csXXXX/marks-top.msg")
			     (bottom-file . "~/teaching/csXXXX/marks-bottom.msg")
			     )))

